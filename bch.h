#define POLY_LSB 0
/* 
   Copyright 2010 Matthieu castet <castet.matthieu@free.fr>

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

/* error correction capability (t) : number of bits than can be corrected */
#define T 4
//#define T 8
//#define T 1
//#define T 2
//#define T 16


/* Galois field order (m). If 'k' is the number of data bits to protect,
   'm' should be chosen such that (k + m*t) <= 2**m - 1.

   For example with t = 4 and m = 13. k <= 2**m - 1 - m*t,
   k <= 8191 - 52
   k <= 8139 bits (~1017 Bytes)
 */
#define POLY_ORDER 13

/**** encoder options ****/
/* speed size tunning */
/* encoding : 
   table size = Round(POLY_ORDER*T)*(2^(BIG_TABLE_ORDER)*BIG_TABLE_SPLIT

   1 <= BIG_TABLE_SPLIT
   8 <= BIG_TABLE_ORDER <= 8
 */
#define BIG_TABLE_ORDER 8
#define BIG_TABLE_SPLIT 4

/**** error decoding options ****/

/* you can select to have a LUT that is encoded on 16 bits instead of 32 bits
   It take less data, but generate slower code
 */
//#define COMPACT_TABLE


/*
   select the size of the LUT that help to decode error.
   It can be 0 for a slow version without LUT.
   For full LUT you need TABLE_ORDER >= 2*t-1
   For full chien LUT you need TABLE_ORDER >= t

   table size = 2^(TABLE_ORDER) * 4
   table size = 2^(TABLE_ORDER) * 2 (COMPACT_TABLE mode)

   If TABLE_ORDER < T, we need 2 LUT (size is * 2)

   for T = 4, full table (order = 2*T-1), we use 512 Bytes
 */
#if T <= 5
/* full table for simba and chien */
#define TABLE_ORDER (2*T-1)
#elif T <= 8
/* full table for chien */
#define TABLE_ORDER T
#else
#define TABLE_ORDER 8
//#error no supported
#endif
//#undef TABLE_ORDER
//#define TABLE_ORDER 3

/**** do not change anything after this ****/

#define BIG_POLY_ORDER (POLY_ORDER*T)
#define BG_SIZE BIG_POLY_ORDER
#include "bignum.h"


void bch_enc_init(void);
bg2_t bch_enc(const char *p, int len);

void bch_correct_init(int len);
int bch_correct(char *p, bg2_t org_poly, bg2_t cal_poly);

#include <time.h>
#define dprintf(x...) printf(x)
//#define dprintf(x...)
#define STAT3 0


